<?php
	require "db.php";
	$is_item= "SELECT id, ToDo_Item FROM items";
	$query_users = $conn->prepare($is_item);
    $query_users->execute();
    $items = $conn->query($is_item);

    $is_completed = "SELECT done FROM item_info";
    $query_done = $conn->prepare($is_completed);
    $query_done->execute();
    $done_item = $conn->query($is_completed);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>ToDo List</title>
		<link href="https://fonts.googleapis.com/css?family=Economica|Indie+Flower" rel="stylesheet">
		<link rel="stylesheet" href="mainCSS.css">

		<meta name="viewport" content="width=device-height, initial-scale=1.0">
	</head>
	<body>
		<div class="todoItem">
			<h1 class="header">ToDo List.</h1>
			
			<?php if(!empty($query_users)): ?>
				<ul class="items">			
					<?php foreach ($query_users as $user_item): ?> 
						<li>						
							<span class="list-item"><?php echo $user_item['ToDo_Item']; ?></span>
							<form action="delete.php" method="post">
								<input type="hidden" name="user_id" value="<?php echo 
								$user_item['id'];?>" />
								<button class="delete-button" type="submit"  value="submit"> Delete!</button>
							</form>
						</li>
						<?php echo '<br>'; ?>
					<?php endforeach; ?>
				</ul>
				<?php endif ?>

			<form class="add-item" action="addItem.php" method="post">
				<input class="input-field" type="text" name="toDo_Item" placeholder="e.g. Meet Ted at 4pm."  autocomplete="off" required>
				<button class='submit-button' type="submit"  value="submit"> Add!</button>
			</form>

	</body>
</html>

<?php
	require "db.php";
	
	$name = $_POST['toDo_Item'];
	$user_item = "INSERT INTO items ( ToDo_Item, items_user ) VALUES (:name, 1 )";
	$query_add = $conn->prepare($user_item);
	$query_add->execute( [':name' => $name ] );

	$user_item_info = "INSERT INTO item_info ( time_added, done, items_user ) VALUES ( NOW(), 0, 1 )";
	$query_add = $conn->prepare($user_item_info);
	$query_add->execute();
	header('Location: ToDo.php');
?>